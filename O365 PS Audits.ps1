﻿# Connect to AD
Import-Module ActiveDirectory
function Find-User {
    param( [string]$user )
    try {
        Get-ADUser $user | Out-Null
        Write-Output $true
    }
    catch {
        Write-Warning "That user does not exist in Active Directory`n"
        Write-Output $false
    }
} #end of function Find-User

function Get-Subordinates {
    param( [string]$manager )

    $subordinateUsers = Get-ADUser $manager -Properties DirectReports | Select-Object @{Label='SubordinateUsername';Expression={$_.DirectReports -replace '^CN=|,.*$'}}
    Write-Output $subordinateUsers.SubordinateUsername
} #end of function Get-Subordinates

function Send-MigrationStatus {
    param($migrationStatus)

    if ([string]$migrationStatus -eq '6') {
        Write-Output "Mailbox in Cloud"
    }
    elseif ([string]$migrationStatus -eq '4') {
        Write-Output "Mailbox being migrated to cloud"
    }
    else {
        Write-Output "Mailbox not in cloud"
    }
} # end of fn Send-MigrationStatus

function Send-LicenseName { 
    param($licenses)

    $arr = @()

    if (!$licenses) {
        $arr += "None"
    }

    else {
        Switch -Wildcard ($licenses) {
            '*Office 365 E3_SPO*' { $arr += "Sharepoint-Only (Vision)" }
            '*Office 365 F1 CORE_EMS_ATP' { $arr += "F1 Core" }
            '*Office 365 E3 EMS_Only*' { $arr += "E3 EMS Only" }
            '*Office 365 E3 CORE_EMS_ATP*' { $arr += "E3 Core" }
            '*Office 365 E3 CORE_PLUS*' { $arr += "E3 Core Plus" }
            '*Office 365 E5 *' { $arr += "E5" }
            '*Power BI Free*' { $arr += "Power BI Free"}
            '*Power BI Pro*' { $arr += "Power BI Pro" }
            '*Project Essentials*' { $arr += "Project Online" }
            '*Project Professional*' { $arr += "Project Professional" }
            '*Project Premium*' { $arr += "Project Premium" }
            '*Visio Online Plan 1*' { $arr += "Visio Online 1" }
            '*Visio Online Plan 2*' { $arr += "Visio Online 2" }
            '*Dynamics 365*' { $arr += "Dynamics 365" }
            '*Windows Defender ATP*' { $arr += "Windows Defender ATP" }
        }
    }

    Write-Output $arr
} #end of fn Send-LicenseName

function Send-SharedMailboxNames {
    param($mailboxIDs)

    $obj = @()
    foreach($m in $mailboxIDs) {
        $mailboxNames = Get-ADGroup $m -Properties Description, Members | Select-Object Description
        $obj += New-Object PSCustomObject -Property @{
            MailboxID = $m
            MailboxName = $mailboxNames.Description
        }
    }

    if($obj) {
        Write-Output "$user has access to the following shared mailboxes: "
        Write-Output $obj
    }
} # end of fn Send-SharedMailboxNames
function Get-UserInfo {
    param( [string] $user )

    $userInfo = Get-ADUser $user -Properties Name, GivenName, Surname, EmailAddress, EmployeeType, Title, Enabled, MsExchRemoteRecipientType, Manager, MemberOf | Select-Object Name, GivenName, Surname, EmailAddress, Title, Enabled, EmployeeType, MsExchRemoteRecipientType, 
        @{
            l="FullName";
            e={
                $_.GivenName + " " + $_.Surname
        }},
        @{
            l="ManagerUsername";
            e={
                $_.Manager -replace "^CN=|,.*$"
            }
        }, 
        @{
            l="O365Groups";
            e={
                $_.MemberOf -like "*AAD LIC*" -replace "^CN=|,.*$"
            }
        }, 
        @{
            l="SharedMailboxes";
            e={
                $_.MemberOf -like "*Exchange_*" -replace "^CN=|,.*$"
            }
        }
    $licenses = Send-LicenseName -licenses $userInfo.O365Groups

    $migrationStatus = Send-MigrationStatus -migrationStatus $userInfo.MsExchRemoteRecipientType

    $sharedMailboxes = Send-SharedMailboxNames -mailboxIDs $userInfo.SharedMailboxes
    
    if (!$userInfo.ManagerUsername) {
        $manager = $false
    }
    else {
        $manager = $userInfo.ManagerUsername
        $subordinates = Get-Subordinates -manager $manager
    }
    
    $output = [PSCustomObject]@{
        Name = $userInfo.Name
        FullName = $userInfo.FullName
        EmployeeType = $userInfo.EmployeeType
        licenses = $licenses
        EmailAddress = $userInfo.EmailAddress
        Title = $userInfo.Title
        Enabled = $userInfo.Enabled
        MigrationStatus = $migrationStatus
        SharedMailboxes = $sharedMailboxes
        Manager = $manager
        Subordinates = $subordinates
    }
    
    Write-Output $output
}
do {
    do {
        #Get the user input (this is the username we want to audit)
        $user = Read-Host "Please enter the username you would like to check"

        #Checks to make sure the username is not null
        if ($user -ne "") {
            #Check to make sure the username is in AD
            $checkValid = Find-User -user $user
            if ($checkValid) {
                #Output the info in a table
                Write-Host "`nUser Info:" -ForegroundColor Cyan

                $userInfo = Get-UserInfo -user $user

                $userInfo | Format-Table Name, FullName, EmailAddress, licenses, MigrationStatus, Title, Enabled, EmployeeType -AutoSize

                #Check user's shared mailboxes
                if (!$userInfo.SharedMailboxes) {
                    Write-Warning "User has no shared mailboxes`n"
                }
                else {
                    Write-Host "Shared Mailboxes:`n" -ForegroundColor Cyan
                    $userInfo.SharedMailboxes | Format-Table MailboxID, MailboxName
                }
                #Some employees do not have a manager specified in AD (rare but it happens). If there IS a manager, get the manager's information and output as a table (same format as user licenses)
                if ($userInfo.Manager) {
                    Write-Host "Manager Info:" -ForegroundColor Cyan
            
                    $managerInfo = Get-UserInfo -user $userInfo.Manager
                    $managerInfo | Format-Table Name, FullName, EmailAddress, licenses, MigrationStatus, Title, Enabled, EmployeeType -AutoSize

                    #Check the subordinates of the manager
                    Write-Host "Getting subordinate info. Please wait, this can take a while." -ForegroundColor Yellow
                    $subordinateInfo = @()
                    if($userInfo.Subordinates) {
                        foreach($s in $userInfo.Subordinates) {
                            $subordinateInfo += Get-UserInfo -user $s
                        }
                    }
                    Write-Host "`nSubordinate Info:" -ForegroundColor Cyan
                    $subordinateInfo | Format-Table Name, FullName, EmailAddress, licenses, MigrationStatus, Title, Enabled, EmployeeType -AutoSize
                } #end if usersmanager not false

                else {
                    Write-Warning "User has no manager in Active Directory`n"
                }
            } # end if checkValid
        } #end if user not null
        else {
            Write-Warning "Username cannot be null`n"
        }
    } until ($user -ne "" -and $checkValid)
    $doAgain = Read-Host "`nWould you like to run again? Y/N"
} until ($doAgain -eq "N")