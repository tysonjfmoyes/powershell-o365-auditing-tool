# Powershell O365 Auditing Tool

This PowerShell script was written for my work so that we could quickly audit Active Directory Users to see if they had Office 365 licenses, if their mailboxes had been moved off-prem into the Cloud, and checked the status of their colleagues

**How The Script Works**

Our AD server has all O365-related licenses starting with "AAD LIC" followed by the specific license. 

This script goes through the following process:
1.  Confirm the username provided is a valid user in Active Directory. 
2.  Once confirmed, grab any info used by the script using Select-Object (username, first & last name, etc.)
3.  Go through the O365 licenses that have been grabbed (they all start with AAD LIC), and put better names on them (for clarity)
4.  Using the MsExchRemoteRecipientType, confirm whether mailbox has been moved to the Cloud or not.
5.  Go through the shared mailboxes and get their proper names (if there are any)
6.  If there is a Manager listed in Active Directory, grab their username and check their subordinates using the DirectReports field in AD
7.  Once all information is gathered, create a PSObject with all the data to create tables to display the information, and output it
8.  Repeat steps 2-5, this time using the manager's username as the parameter 
9.  Finally, repeat steps 2-5 for all subordinates of the manager

Once all the information is provided, it can be copy-pasted into our call tracking system and sent to the relevant parties.
The whole script is also loopable, asking the user if they would like to run another search.

**What I Learned**
* This was my first PowerShell script, so I learned a fair amount about PowerShell scripting and its syntax
* How to run simple & complex Active Directory queries through PowerShell
* I learned that all users have read access to Active Directory. Early on in development I had required admin credentials to run the script, but then I learned that I didn't need them
* Thanks to r/powershell, I learned that using `return` is not recommended. As such, I have updated the script to use `Write-Output` instead! 

**How To Run**

You would need an Active Directory server, as well as the Active Directory PowerShell Module installed on the computer you are running the script from
If you have those, right-click the .ps1 file and select "Run with PowerShell"

**Plans For The Future**
* [x]  The script uses a filter to select the subordinates, but I have learned that managers have a DirectReports field in AD. Using this instead of a filter should speed up the process for checking subordinates.
* [x]  Tweak script so that only one Active Directory query is done, rather than querying over and over for the same user. 
* [x]  Replace 'return' with Write-Output (or a variant thereof) to better match standards
* [x]  Replace the comments I just deleted re-writing everything. 

Big thanks to the folks at r/powershell for the feedback!